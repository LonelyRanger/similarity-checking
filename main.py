import docx
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import numpy as np
from gensim import corpora, models, similarities
from simtext import similarity

f1 = docx.Document("template.docx")
for para in f1.paragraphs:
    f1_text = para.text

f2 = docx.Document("checking.docx")
for para in f2.paragraphs:
    f2_text = para.text

print(f1_text)
print(f2_text)
sim1 = similarity()
res = sim1.compute(f1_text, f2_text)
print(res)


def split(filename):
    file = docx.Document(filename)
    file_text = []
    text_tokenized = []

    for para in file.paragraphs:
        file_text.append(para.text)
    # print(file_text)
    for data in file_text:
        word_tokenized = word_tokenize(data)
        for word in word_tokenized:
            text_tokenized.append(word)
    # print(text_tokenized)

    english_punctuations = [',', '.', ':', ';', '?', '!', '(', ')', '[', ']', '@', '&', '#', '%', '$', '{', '}', '--',
                            '-']
    punctuation_filtered = [w for w in text_tokenized if w not in english_punctuations]

    stop_words = set(stopwords.words('english'))
    filtered_text = [w for w in punctuation_filtered if w not in stop_words]
    return filtered_text


def word_frequency(text, corpus):
    text_dic = {}
    for word in corpus:
        text_dic[word] = 0
    text_vec = []
    for word in text:
        if word in text_dic:
            text_dic[word] = text_dic.get(word)+1
    for key in text_dic:
        text_vec.append(text_dic[key])
    return text_vec


def cosine_similarity(x, y):
    dot_product = 0
    square_sum_x = 0
    square_sum_y = 0
    for i in range(len(x)):
        dot_product += x[i] * y[i]
        square_sum_x += x[i] * x[i]
        square_sum_y += y[i] * y[i]
    cos = dot_product / (np.sqrt(square_sum_x)*np.sqrt(square_sum_y))
    return cos


template_list = split("template.docx")
template_text = [word.lower() for word in template_list if isinstance(word, str)]
checking_list = split("checking.docx")
checking_text = [word.lower() for word in checking_list if isinstance(word, str)]
whole_text = template_text + checking_text
merged_text = list(set(whole_text))
merged_text.sort()
print(merged_text)
wob = merged_text

template_vec = word_frequency(template_text, wob)
print(template_vec)
checking_vec = word_frequency(checking_text, wob)
print(checking_vec)

similarity = cosine_similarity(template_vec, checking_vec)
print(similarity)

if similarity > 0.8:
    print("Pass")
elif similarity > 0.2:
    print("Manual checking required")
else:
    print("Fail")


# dictionary = corpora.Dictionary(text_list)
# corpus = [dictionary.doc2bow(text) for text in text_list]
# #template_vec = dictionary.doc2bow(template_text)
# checking_vec = dictionary.doc2bow(checking_text)
# print(checking_vec)
# print(corpus)

# tfidf = models.TfidfModel(corpus)
# for i in tfidf[corpus]:
#     print(i)
# index = similarities.SparseMatrixSimilarity(tfidf[corpus], num_features=len(dictionary.keys()))
# sim = index[tfidf[checking_vec]]
# print(sim)
